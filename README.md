# Otaku Store Inventory

A Mock store inventory app originally created in 2017 & updated in 2024 to run locally.

### 🗒️Overview

"This project is a chance for you to combine and practice everything you learned in this section of the Nanodegree program. 
You will be making an app to track a store's inventory.

The goal is to design and create the structure of an Inventory App which would allow a store to keep track of its inventory of products. 
The app will need to store information about price, quantity available, supplier, and a picture of the product. 
It will also need to allow the user to track sales and shipments and make it easy for the user to order more from the listed supplier."
```
    - Storing information in a SQLite database
    - Integrating Android’s file storage systems into that database
    - Presenting information from files and SQLite databases to users
    - Updating information based on user input.
    - Creating intents to other apps using stored information.
```

### 🏁 Getting Started

#### Prerequisites

To successfully build and run this app locally in Android Studio, you need to following:

| Config       | Details                   |
|--------------|---------------------------|
| Android SDK  | 8.0 (Oreo) API 26         |
| Gradle /     | v7.3.3                    |
| $agp_version | v7.2.0                    |
| Gradle JDK   | Jetbrains runtime v17.0.7 |

In file `./build.gradle` *buildscript.repositories* & *allprojects.repositories* contain:
```
google()
mavenCentral()
```

### 📷 Screenshots

| Empty State                                                          |              Main Menu FAB (Floating Action Button)               |                                                          Store Products |
|:----------------------------------------------------------------------:|:-----------------------------------------------------------------:|:------------------------------------------------------------------------:|
| ![Empty state](public/screens/empty-state.webp){width=300px} | ![FAB menu](public/screens/fab-menu.webp){width=300px} | ![Inventory list](public/screens/inventory-list.webp){width=300px} |

| Add New Product                                                    |                            Edit Product                            |                                                       Upload Product Image |
|:----------------------------------------------------------------------:|:------------------------------------------------------------------:|:---------------------------------------------------------------------------:|
| ![Add new product](public/screens/create-item.webp){width=300px} | ![Edit product](public/screens/edit-item.webp){width=300px} | ![Upload product image](public/screens/upload-item-image.webp){width=300px} |

| Contact Supplier                                                          |                  Product FAB (Floating Action Button)                  |                                              Delete Product Confirmation Dialog box |
|:---------------------------------------------------------------------------:|:----------------------------------------------------------------------:|:----------------------------------------------------------------------------:|
| ![Contact supplier](public/screens/contact-supplier-dialog.webp){width=300px} | ![Product FAB](public/screens/item-fab-menu.webp){width=300px} | ![Delete product dialog](public/screens/delete-item-dialog.webp){width=300px} |

_Disclaimer: None of the images used to create this app belong to me._
